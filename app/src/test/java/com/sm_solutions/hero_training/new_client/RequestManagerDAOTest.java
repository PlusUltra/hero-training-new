package com.sm_solutions.hero_training.new_client;

import com.sm_solutions.hero_training.new_client.exception.ResponseException;

import org.junit.Test;

import static com.sm_solutions.hero_training.new_client.RequestManagerDAO.EMPTY_RESPONSE;

/**
 * Created by sgsr38213 on 30.01.2018.
 */
public class RequestManagerDAOTest {

    private RequestManagerDAO requestManagerDAO = new RequestManagerDAO();

    @Test()
    public void shouldCheckResponseForEmptyBody() throws Exception {
        String response = "{response}";
        RequestManagerDAO.checkResponseForEmptyBody(response);
    }

    @Test(expected = ResponseException.class)
    public void throwExceptionIfResponseIsEmpty() throws Exception {
        RequestManagerDAO.checkResponseForEmptyBody(EMPTY_RESPONSE);
    }

}