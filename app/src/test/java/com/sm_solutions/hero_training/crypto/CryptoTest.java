package com.sm_solutions.hero_training.crypto;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by ultra on 06.03.18.
 */
public class CryptoTest {
    @Test
    public void encryptAndDecrypt() throws Exception {
        String origin = "abcdef";
        String ecrypted = Crypto.encrypt(origin);
        System.out.println("Encrypted : " + ecrypted);
        assertTrue(ecrypted.length() >= origin.length());
        assertTrue(Crypto.decrypt(ecrypted).equals(origin));
    }

    @Test
    public void encryptAndDecryptUTF() throws Exception {
        String origin = "ЫЫЫЫЫЫфывфыв";
        String ecrypted = Crypto.encrypt(origin);
        System.out.println("Encrypted : " + ecrypted);
        assertTrue(ecrypted.length() >= origin.length());
        assertTrue(Crypto.decrypt(ecrypted).equals(origin));
    }

}