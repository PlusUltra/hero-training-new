package com.sm_solutions.hero_training;

import com.annimon.stream.Stream;
import com.google.gson.Gson;
import com.sm_solutions.hero_training.structures.Exercise;
import com.sm_solutions.hero_training.structures.dto.DtoUtils;
import com.sm_solutions.hero_training.structures.dto.QuestInfoDto;
import com.sm_solutions.hero_training.structures.dto.StatisticDetailsDto;
import com.sm_solutions.hero_training.structures.dto.TrainingInfoDto;
import com.sm_solutions.hero_training.structures.dto.UserTokenDto;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by sgsr38213 on 22.02.2018.
 */
public class GsonTest {

    @Test
    public void checkParamsToJson() throws Exception {
        Map<String, Object> params = getRequestParams();
        String json = new Gson().toJson(params);
        System.out.print(json);

        for(String s : params.keySet()){
            assertTrue(json.contains(s));
        }
    }

    @Test
    public void checkDtoToJson() throws Exception {
        UserTokenDto dto = new UserTokenDto("old", "new");
        String json = new Gson().toJson(dto);
        System.out.print(json);

        assertTrue(json.contains("old"));
    }

    @Test
    public void checkTrainingDtoToJson() throws Exception {
        String token = "token1";
        Exercise exercise1 = new Exercise();
        Exercise exercise2 = new Exercise();
        Exercise exercise3 = new Exercise();
        Exercise exercise4 = new Exercise();
        exercise1.setId(1);
        exercise2.setId(2);
        exercise3.setId(3);
        exercise3.setId(4);

        exercise1.setDescription("long description");

        exercise1.setExp(100);
        exercise2.setExp(200);
        exercise3.setExp(300);
        exercise4.setExp(0);

        List<Exercise> exercises = Arrays.asList(exercise1, exercise2, exercise3, exercise4);
        TrainingInfoDto dto = DtoUtils.getTrainingInfoDto(exercises, token);

        String json = new Gson().toJson(dto);
        System.out.print(json);

        assertTrue(exercises.get(exercises.indexOf(exercise1)).getDescription() != null);
        assertTrue(dto.getDetails().size() == 3);
        assertTrue(json.contains(token));
        Stream.of(dto.getDetails()).forEach(e -> assertTrue(json.contains(String.valueOf(e.getExp()))));
    }

    private Map<String, Object> getRequestParams(){
        Map<String, Object> params = new HashMap<>();
        params.put("name", "name");
        params.put("password", "password");
        params.put("sex", "1");
        params.put("token", "tokennn");
        return params;
    }

    @Test
    public void questDtoTest() throws Exception {
        QuestInfoDto dto = new QuestInfoDto();
        dto.setToken("token");
        dto.setExp(12);
        dto.setCount(10);
        dto.setTraining_id(100);

        String json = new Gson().toJson(dto);
        System.out.print(json);

        assertTrue(json.contains("token"));
    }

    @Test
    public void expToDateDtoTest() throws Exception {
        String response = "{\"training\":[{\"exp\":10,\"date\":150000}, {\"exp\":11,\"date\":150001}]," +
                " \"quests\":[{\"exp\":10,\"date\":150000}, {\"exp\":11,\"date\":150001}]}";
        StatisticDetailsDto detailsDto = new Gson().fromJson(response, StatisticDetailsDto.class);

        assertTrue(detailsDto.getTraining().get(0).getExp() == 10);
        assertTrue(detailsDto.getQuests().get(1).getExp() == 11);
    }
}