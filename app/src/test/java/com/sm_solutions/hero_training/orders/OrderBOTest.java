package com.sm_solutions.hero_training.orders;

import com.sm_solutions.hero_training.orders.exception.BOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by Arthas on 03.09.2017.
 */
public class OrderBOTest {
    @Mock OrderDAO mOrderDAO;
    private OrderBO bo;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        bo = new OrderBO();
        bo.setOrderDAO(mOrderDAO);
    }

    @Test
    public void placeOrderPositive() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.create(order)).then(o -> 1);
        assertTrue(bo.placeOrder(order));
        verify(mOrderDAO).create(order);
    }

    @Test
    public void placeOrderNegative() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.create(order)).then(o -> 0);
        assertFalse(bo.placeOrder(order));
        verify(mOrderDAO).create(order);
    }

    @Test(expected = BOException.class)
    public void placeOrderThowsBOException() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.create(order)).thenThrow(SQLException.class);
        boolean result = bo.placeOrder(order);
    }

    @Test
    public void cancelOrderShouldCancelOrder() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.read(123)).thenReturn(order);
        when(mOrderDAO.update(order)).thenReturn(1);
        boolean result = bo.cancelOrder(123);

        assertTrue(result);
        verify(mOrderDAO).read(123);
        verify(mOrderDAO).update(order);
    }

    @Test
    public void cancelOrderShouldNotCancelOrder() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.read(123)).thenReturn(order);
        when(mOrderDAO.update(order)).thenReturn(0);
        boolean result = bo.cancelOrder(123);

        assertFalse(result);
        verify(mOrderDAO).read(123);
        verify(mOrderDAO).update(order);
    }

    @Test(expected = BOException.class)
    public void cancelOrderShouldThrowExceptionOnRead() throws SQLException, BOException {
        when(mOrderDAO.read(123)).thenThrow(SQLException.class);
        boolean result = bo.cancelOrder(123);
    }

    @Test(expected = BOException.class)
    public void cancelOrderShouldThrowExceptionOnUpdate() throws SQLException, BOException {
        Order order = new Order();
        when(mOrderDAO.read(123)).thenReturn(order);
        when(mOrderDAO.update(order)).thenThrow(SQLException.class);
        bo.cancelOrder(123);
    }

    @Test
    public void cancelOrder() throws Exception {

    }

    @Test
    public void deleteOrder() throws Exception {

    }

}