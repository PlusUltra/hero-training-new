package com.sm_solutions.hero_training;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.ui.ConstantsFragment;
import com.sm_solutions.hero_training.ui.fragment.FragmentMain;
import com.sm_solutions.hero_training.ui.fragment.FragmentQuests;
import com.sm_solutions.hero_training.ui.fragment.FragmentTraining;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.isFragmentToReplace;

/**
 * Created by ultra on 05.03.18.
 */

public class FragmentTransactionUtils {

    private MainActivity activity;

    public FragmentTransactionUtils(MainActivity activity) {
        this.activity = activity;
    }

    public void cleanBackStack(){
        try {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
                fragmentManager.popBackStack();
            }
        }catch (Exception e){}
    }

    public void addNewFragment(Fragment fragment, String currentFragmentTag, String newTag){
        Fragment currentFragment;
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction
                .addToBackStack(currentFragmentTag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.frameContainer, fragment, newTag);
        if ((currentFragment = activity.getSupportFragmentManager().findFragmentById(R.id.frameContainer))
                != null) {
            transaction.hide(currentFragment);
        }
        transaction.commit();
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        String tag = "";
        switch(menuItem.getItemId()) {
            case R.id.nav_main:
                fragment = new FragmentMain();
                tag = ConstantsFragment.FRAGMENT_MAIN;
                break;

            case R.id.nav_training:
                fragment = new FragmentTraining();
                tag = ConstantsFragment.FRAGMENT_TRAINING;
                break;

            case R.id.nav_quests:
                fragment = new FragmentQuests();
                tag = ConstantsFragment.FRAGMENT_QUESTS;
                break;
        }

        cleanBackStack();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).commit();

        menuItem.setChecked(true);
    }

    public void openNavigationItem(NavigationView navigationView, int item){
        MenuItem menuItem = navigationView.getMenu().getItem(item);
        selectDrawerItem(menuItem);
    }

    public boolean shouldReplaceWithMainFragment(){
        FragmentManager manager = activity.getSupportFragmentManager();
        return Stream.of(manager.getFragments()).anyMatch(f ->
                f != null && f.isVisible() && isFragmentToReplace(f.getTag()));
    }
}