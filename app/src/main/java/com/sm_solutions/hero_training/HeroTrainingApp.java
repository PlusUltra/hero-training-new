package com.sm_solutions.hero_training;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.sm_solutions.hero_training.di.components.AppComponent;
import com.sm_solutions.hero_training.di.components.DaggerAppComponent;
import com.sm_solutions.hero_training.di.components.MainActivityComponent;
import com.sm_solutions.hero_training.di.modules.AppModule;
import com.sm_solutions.hero_training.di.modules.FragmentModule;
import com.sm_solutions.hero_training.di.modules.MainActivityModule;
import com.sm_solutions.hero_training.di.modules.NetModule;
import com.sm_solutions.hero_training.ui.fragment.FragmentModel;

import io.realm.Realm;

/**
 * Created by Arthas on 06.09.2017.
 */

public class HeroTrainingApp extends Application {

    private AppComponent appComponent;
    private MainActivityComponent mainActivityComponent;

    public void onCreate() {
        super.onCreate();
        //MultiDex.install(this);
        FirebaseApp.initializeApp(this);
        Realm.init(this);
        initAppComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .build();
        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public void injectMainActivityComponent(MainActivity activity){
        mainActivityComponent =  appComponent.plus(new MainActivityModule(activity));
        mainActivityComponent.inject(activity);
    }

    public void injectFragmentModelComponent(FragmentModel fragmentModel) {
        mainActivityComponent.plusFragmentComponent(
                new FragmentModule(fragmentModel)).inject(fragmentModel);
    }

    public static HeroTrainingApp get(Context context) {
        return (HeroTrainingApp) context.getApplicationContext();
    }
}
