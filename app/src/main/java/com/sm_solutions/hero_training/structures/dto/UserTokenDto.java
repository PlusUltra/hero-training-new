package com.sm_solutions.hero_training.structures.dto;

import java.io.Serializable;

public class UserTokenDto implements Serializable{

    private String oldToken;
    private String newToken;

    public UserTokenDto(String oldToken, String newToken) {
        this.oldToken = oldToken;
        this.newToken = newToken;
    }

    public UserTokenDto() {
    }

    public String getOldToken() {
        return oldToken;
    }

    public void setOldToken(String oldToken) {
        this.oldToken = oldToken;
    }

    public String getNewToken() {
        return newToken;
    }

    public void setNewToken(String newToken) {
        this.newToken = newToken;
    }
}
