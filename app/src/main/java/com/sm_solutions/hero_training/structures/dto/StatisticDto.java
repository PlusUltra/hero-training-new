package com.sm_solutions.hero_training.structures.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sgsr38213 on 23.02.2018.
 */

public class StatisticDto implements Parcelable {

    private int exp;
    private long date;

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.exp);
        dest.writeLong(this.date);
    }

    public StatisticDto() {
    }

    protected StatisticDto(Parcel in) {
        this.exp = in.readInt();
        this.date = in.readLong();
    }

    public static final Parcelable.Creator<StatisticDto> CREATOR = new Parcelable.Creator<StatisticDto>() {
        @Override
        public StatisticDto createFromParcel(Parcel source) {
            return new StatisticDto(source);
        }

        @Override
        public StatisticDto[] newArray(int size) {
            return new StatisticDto[size];
        }
    };
}
