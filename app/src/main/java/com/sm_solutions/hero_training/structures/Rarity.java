package com.sm_solutions.hero_training.structures;

/**
 * Created by sgsr38213 on 03.01.2018.
 */

public class Rarity {

    private static final String RARITY_GOLD = "gold";
    private static final String RARITY_SILVER = "silver";
    private static final String RARITY_BRONZE = "bronze";

    private static final float RARITY_GOLD_MULTIPLIER = 2.5f;
    private static final float RARITY_SILVER_MULTIPLIER = 2.0f;
    private static final float RARITY_BRONZE_MULTIPLIER = 1.5f;

    public static void setExpAndCountDependOnRarity(Exercise exercise){
        switch (exercise.rarity){
            case RARITY_GOLD:{
                makeProduct(exercise, RARITY_GOLD_MULTIPLIER);
                break;
            }

            case RARITY_SILVER:{
                makeProduct(exercise, RARITY_SILVER_MULTIPLIER);
                break;
            }

            case RARITY_BRONZE:{
                makeProduct(exercise, RARITY_BRONZE_MULTIPLIER);
                break;
            }
        }
    }

    private static void makeProduct(Exercise exercise, float rarityMultiplier) {
        exercise.exp *= rarityMultiplier;
        exercise.count *= rarityMultiplier;
    }
}
