package com.sm_solutions.hero_training.structures.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sgsr38213 on 23.02.2018.
 */

public class StatisticDetailsDto implements Parcelable {

    private List<StatisticDto> training;
    private List<StatisticDto> quests;

    public List<StatisticDto> getTraining() {
        return training;
    }

    public void setTraining(List<StatisticDto> training) {
        this.training = training;
    }

    public List<StatisticDto> getQuests() {
        return quests;
    }

    public void setQuests(List<StatisticDto> quests) {
        this.quests = quests;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.training);
        dest.writeList(this.quests);
    }

    public StatisticDetailsDto() {
    }

    protected StatisticDetailsDto(Parcel in) {
        this.training = new ArrayList<>();
        in.readList(this.training, StatisticDto.class.getClassLoader());
        this.quests = new ArrayList<>();
        in.readList(this.quests, StatisticDto.class.getClassLoader());
    }

    public static final Parcelable.Creator<StatisticDetailsDto> CREATOR = new Parcelable.Creator<StatisticDetailsDto>() {
        @Override
        public StatisticDetailsDto createFromParcel(Parcel source) {
            return new StatisticDetailsDto(source);
        }

        @Override
        public StatisticDetailsDto[] newArray(int size) {
            return new StatisticDetailsDto[size];
        }
    };

    public List<Entry> getTrainingEntry(){
        return getEntryFromExpToDateDto(training);
    }

    public List<Entry> getQuestsEntry(){
        return getEntryFromExpToDateDto(quests);
    }

    private List<Entry> getEntryFromExpToDateDto(List<StatisticDto> dtos) {
        return Stream.of(dtos).map(statisticDto ->
                new Entry(statisticDto.getDate(), statisticDto.getExp()))
                .collect(Collectors.toList());
    }
}
