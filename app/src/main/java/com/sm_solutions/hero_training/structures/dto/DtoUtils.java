package com.sm_solutions.hero_training.structures.dto;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.structures.Exercise;

import java.util.List;

/**
 * Created by ultra on 25.03.18.
 */

public class DtoUtils {

    public static TrainingInfoDto getTrainingInfoDto(List<Exercise> exercises, String token){
        TrainingInfoDto dto = new TrainingInfoDto();
        List<Exercise> exercisesCopy = Stream.of(exercises).filter(e -> e.getExp() != 0)
                .map(DtoUtils::exerciseWithOnlyValuesForDto).toList();
        dto.setDetails(exercisesCopy);
        dto.setToken(token);
        return dto;
    }

    private static Exercise exerciseWithOnlyValuesForDto(Exercise exercise){
        Exercise exerciseDto = new Exercise();
        exerciseDto.setId(exercise.getId());
        exerciseDto.setCount(exercise.getCount());
        exerciseDto.setExp(exercise.getExp());
        exerciseDto.setRarity(exercise.getRarity());
        exerciseDto.setDescription(null);
        exerciseDto.setLink(null);
        exerciseDto.setType(null);
        return exerciseDto;
    }
}
