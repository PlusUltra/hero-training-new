package com.sm_solutions.hero_training.structures;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class User {

    private int id;
    private String name;
    private short sex;
    private int exp;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public short getSex() {
        return sex;
    }

    public int getExp() {
        return exp;
    }

    public void addExp(int exp) {
        this.exp += exp;
    }

    public void copyValues(User user){
        id = user.id;
        sex = user.sex;
        exp = user.exp;
        try {
            name = URLDecoder.decode(user.name, "UTF-8");
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
