package com.sm_solutions.hero_training.structures;

import android.os.Parcel;
import android.os.Parcelable;


public class Exercise implements Parcelable{
    public static final String RARITY_NONE = "none";

    int id;
    String type = "";
    String description = "";
    short count;
    int exp;
    String link = "";
    String rarity = RARITY_NONE;

    public Exercise() {
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public short getCount() {
        return count;
    }

    public int getExp() {
        return exp;
    }

    public void removeExp() {
        this.exp = 0;
    }

    public String getLink() {
        return link;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public void setCountDependOnLevel(int level){
        count = (short) (((level - 1)*0.2 + 1) * count);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCount(short count) {
        this.count = count;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setExpAndCountDependOnRarity(){
        Rarity.setExpAndCountDependOnRarity(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.type);
        dest.writeInt(this.count);
        dest.writeInt(this.exp);
        dest.writeString(this.link);
        dest.writeString(this.rarity);
    }

    protected Exercise(Parcel in) {
        this.id = in.readInt();
        this.type = in.readString();
        this.count = (short) in.readInt();
        this.exp = in.readInt();
        this.link = in.readString();
        this.rarity = in.readString();
    }

    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel source) {
            return new Exercise(source);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };
}
