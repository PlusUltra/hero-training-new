package com.sm_solutions.hero_training.structures;

/**
 * Created by sgsr38213 on 07.12.2017.
 */

public class AccessToken {

    String token;

    public AccessToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
