package com.sm_solutions.hero_training.structures;

/**
 * Created by ultra on 14.12.17.
 */

public class Experience {

    private int totalExp;
    private int nextLevelExp;
    private double counterOfProgression;
    private int level;
    private int currentExp;

    public boolean updateTotalExp(int totalExp){
        this.totalExp = totalExp;
        int oldLevel = level;
        recount();
        return (level - oldLevel > 0) && oldLevel != 0;
    }

    private void recount() {
        setUpProgressionCounter();
        setUpLevel();
        setUpNextLevelExp();
        setUpCurrentExp();
    }

    private void setUpProgressionCounter() {
        counterOfProgression = (-7 + Math.sqrt(49 + 8*totalExp/25))/2;
    }

    private void setUpLevel() {
        level = (int) (counterOfProgression + 1);
    }

    private void setUpNextLevelExp() {
        nextLevelExp = ((int) counterOfProgression)*25 + 100;
    }

    private void setUpCurrentExp() {
        currentExp = totalExp - ((level-1)*(200 + (level-2)*25)/2);
    }

    public int getNextLevelExp() {
        return nextLevelExp;
    }

    public int getLevel() {
        return level;
    }

    public int getCurrentExp() {
        return currentExp;
    }
}
