package com.sm_solutions.hero_training.structures.dto;



import com.sm_solutions.hero_training.structures.Exercise;

import java.io.Serializable;
import java.util.List;

public class TrainingInfoDto implements Serializable{

    private List<Exercise> details;
    private String token;

    public List<Exercise> getDetails() {
        return details;
    }

    public void setDetails(List<Exercise> details) {
        this.details = details;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
