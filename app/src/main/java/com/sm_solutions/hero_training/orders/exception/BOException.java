package com.sm_solutions.hero_training.orders.exception;

import java.sql.SQLException;


public class BOException extends Exception {

    public BOException(SQLException e) {
        super(e);
    }
}
