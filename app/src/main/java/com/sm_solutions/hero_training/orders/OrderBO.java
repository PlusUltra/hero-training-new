package com.sm_solutions.hero_training.orders;

import com.sm_solutions.hero_training.orders.exception.BOException;

import java.sql.SQLException;

/**
 * Created by Arthas on 03.09.2017.
 */

public class OrderBO {
    private OrderDAO mOrderDAO;

    public boolean placeOrder(Order order) throws BOException {
        int result;
        try {
            result = mOrderDAO.create(order);
        } catch (SQLException e) {
            throw new BOException(e);
        }
        return result != 0;
    }

    public boolean cancelOrder(int id) throws BOException{
        int result = 0;
        try {
            Order order = mOrderDAO.read(id);
            order.setStatus("cancelled");
            result = mOrderDAO.update(order);
        } catch (SQLException e) {
            throw new BOException(e);
        }
        return result != 0;
    }

    public boolean deleteOrder(int id) throws BOException{
        int result;

        try {
            result = mOrderDAO.delete(id);
        } catch (SQLException e) {
            throw new BOException(e);
        }
        return result != 0;
    }

    public OrderDAO getOrderDAO() {
        return mOrderDAO;
    }

    public void setOrderDAO(OrderDAO orderDAO) {
        mOrderDAO = orderDAO;
    }
}
