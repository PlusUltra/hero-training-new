package com.sm_solutions.hero_training.orders;

/**
 * Created by Arthas on 03.09.2017.
 */

class Order {
    private int mId;
    private String mStatus;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
