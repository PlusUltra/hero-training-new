package com.sm_solutions.hero_training.new_client.exception;

/**
 * Created by Arthas on 13.09.2017.
 */

public class JsonException extends Exception {

    public JsonException(String message) {
        super(message);
    }

    public JsonException(Throwable cause) {
        super(cause);
    }
}
