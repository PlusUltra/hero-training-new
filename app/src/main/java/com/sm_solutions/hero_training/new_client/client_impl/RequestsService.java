package com.sm_solutions.hero_training.new_client.client_impl;

import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by ultra on 03.12.17.
 */

public interface RequestsService {

    @GET()
    Single<ResponseBody> makeGetRequestWithUrlEndpoint(@Url String url);

    @POST("/users/add")
    Single<ResponseBody> createNewUser(@Body RequestBody requestBody);

    @POST("/users/recovery")
    Single<ResponseBody> recoveryUser(@Body RequestBody requestBody);

    @POST("/users/update_token")
    Single<ResponseBody> updateToken(@Body RequestBody requestBody);

    @POST("/training/finish")
    Single<ResponseBody> finishTraining(@Body RequestBody requestBody);

    @POST("/quests/finish")
    Single<ResponseBody> finishQuest(@Body RequestBody requestBody);
}
