package com.sm_solutions.hero_training.new_client.exception;

/**
 * Created by Arthas on 13.09.2017.
 */

public class ParseException extends Exception {

    public ParseException(String reason){
        super(reason);
    }
}
