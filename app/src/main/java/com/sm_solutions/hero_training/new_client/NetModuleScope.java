package com.sm_solutions.hero_training.new_client;

import javax.inject.Scope;

/**
 * Created by Arthas on 06.09.2017.
 */

@Scope
public @interface NetModuleScope {
}
