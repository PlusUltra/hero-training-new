package com.sm_solutions.hero_training.new_client;

import io.reactivex.Single;
import okhttp3.ResponseBody;

/**
 * Created by ultra on 03.12.17.
 */

public interface MakeRequest {

    Single<ResponseBody> makeRequest();
}
