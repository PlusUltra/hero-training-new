package com.sm_solutions.hero_training.new_client.client_impl;

import com.google.gson.Gson;
import com.sm_solutions.hero_training.new_client.MakeRequest;
import com.sm_solutions.hero_training.new_client.RequestManagerDAO;
import com.sm_solutions.hero_training.structures.Exercise;
import com.sm_solutions.hero_training.structures.User;
import com.sm_solutions.hero_training.structures.dto.QuestInfoDto;
import com.sm_solutions.hero_training.structures.dto.StatisticDetailsDto;
import com.sm_solutions.hero_training.structures.dto.TrainingInfoDto;
import com.sm_solutions.hero_training.structures.dto.UserTokenDto;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RequestManager {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private RequestsService service;
    private Gson gson = new Gson();

    public RequestManager(RequestsService service) {
        this.service = service;
    }

    /* GET REQUESTS*/

    public Single<User> getUserInstanceRequest(String token){
        return  getRequestWithSingleObjectResult("/users/" + token, User.class);
    }

    public Single<StatisticDetailsDto> getExpStatistic(String token) {
        return getRequestWithSingleObjectResult("/users/statistic/" + token,
                StatisticDetailsDto.class);
    }

    public Single<List<Exercise>> getQuestsList(String token){
        return getRequestWithListObjectResult("/quests/" + token, Exercise.class);
    }


    public Single<List<Exercise>> getTrainingList(String token){
        return getRequestWithListObjectResult("/training/" + token, Exercise.class);
    }

    /** POST REQUESTS*/

    /* User-result requests*/
    public Single<User> updateToken(UserTokenDto userTokenDto) {
        return paramsRequestWithSingleObjectResult(() ->
                service.updateToken(getBodyFromObject(userTokenDto)), User.class);
    }

    public Single<User> addNewUser(Map<String, Object> params) {
        return paramsRequestWithSingleObjectResult(() ->
                service.createNewUser(getBodyFromObject(params)), User.class);
    }

    public Single<User> recoveryUser(Map<String, Object> params) {
        return paramsRequestWithSingleObjectResult(() ->
                service.recoveryUser(getBodyFromObject(params)), User.class);
    }


    /* Finish requests */
    public Single<JSONObject> finishTraining(TrainingInfoDto params){
        return getJson(() -> service.finishTraining(getBodyFromObject(params)));
    }

    public Single<JSONObject> finishQuest(QuestInfoDto params){
        return getJson(() -> service.finishQuest(getBodyFromObject(params)));
    }

/*Model*/

    private <T> Single<T> getRequestWithSingleObjectResult(String endpoint, Class<T> clazz) {
        return paramsRequestWithSingleObjectResult(()->
                service.makeGetRequestWithUrlEndpoint(endpoint), clazz);
    }

    private  <T> Single<T> paramsRequestWithSingleObjectResult(MakeRequest request, Class<T> clazz){
        return RequestManagerDAO.single(request, clazz);
    }

    private <T> Single<List<T>> getRequestWithListObjectResult(String endpoint, Class<T> clazz) {
        return RequestManagerDAO.list(()->
                service.makeGetRequestWithUrlEndpoint(endpoint), clazz);
    }

    private Single<JSONObject> getJson(MakeRequest request){
        return RequestManagerDAO.json(request);
    }

    private RequestBody getBodyFromObject(Object object){
        return RequestBody.create(JSON, gson.toJson(object));
    }
}
