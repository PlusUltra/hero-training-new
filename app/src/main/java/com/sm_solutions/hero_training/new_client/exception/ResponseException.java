package com.sm_solutions.hero_training.new_client.exception;

/**
 * Created by sgsr38213 on 07.12.2017.
 */

public class ResponseException extends Exception {

    public ResponseException(Throwable cause) {
        super(cause);
    }

    public ResponseException(String emptyResponse) {
        super(emptyResponse);
    }
}
