package com.sm_solutions.hero_training.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sm_solutions.hero_training.R;

/**
 * Created by sgsr38213 on 19.10.2017.
 */

public class FragmentLoading extends FragmentModel{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_loading, container, false);
        return v;
    }

    @Override
    protected String getTitle() {
        return getString(R.string.fragment_loading);
    }
}
