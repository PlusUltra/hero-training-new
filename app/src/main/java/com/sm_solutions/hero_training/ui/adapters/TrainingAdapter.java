package com.sm_solutions.hero_training.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.Exercise;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ultra on 15.12.17.
 */

public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.TrainingViewHolder>{

    private List<Exercise> exerciseList;

    public TrainingAdapter(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
        setHasStableIds(true);
    }

    @Override
    public TrainingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_training , viewGroup, false);
        return new TrainingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TrainingViewHolder viewHolder, int i) {
        Exercise exercise = exerciseList.get(i);

        viewHolder.tvNumber.setText(String.valueOf(i+1));
        viewHolder.tvType.setText(exercise.getType());
        viewHolder.tvCount.setText(String.valueOf(exercise.getCount()));
        viewHolder.tvExp.setText(String.valueOf(exercise.getExp()));
        if(exercise.getExp() == 0)
            viewHolder.itemView.setBackgroundResource(R.color.semiRed);
        else
            viewHolder.itemView.setBackgroundResource(android.R.color.transparent);
    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    class TrainingViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvItemTrainingNumber) TextView tvNumber;
        @BindView(R.id.tvItemTrainingType) TextView tvType;
        @BindView(R.id.tvItemTrainingCount) TextView tvCount;
        @BindView(R.id.tvItemTrainingExp) TextView tvExp;

        public TrainingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setExerciseList(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }
}
