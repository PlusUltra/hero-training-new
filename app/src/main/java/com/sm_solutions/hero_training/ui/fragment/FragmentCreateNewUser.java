package com.sm_solutions.hero_training.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.crypto.Crypto;
import com.sm_solutions.hero_training.structures.User;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_CREATE_NEW_USER;
import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_RECOVERY;

/**
 * Created by sgsr38213 on 19.10.2017.
 */

public class FragmentCreateNewUser extends FragmentModel{

    @BindView(R.id.etFragmentStartName) EditText editTextName;
    @BindView(R.id.etFragmentStartPassword) EditText editTextPassword;
    @BindView(R.id.etFragmentStartPasswordConfirm) EditText editTextPasswordConfirm;
    @BindView(R.id.rgFragmentStartSex) RadioGroup radioGroup;
    private View mainView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mainView = inflater.inflate(R.layout.fragment_start, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @OnClick(R.id.btnFragmentStartConfirm)
    public void submit() {
        makeNewUserRequest();
    }

    @OnClick(R.id.btnFragmentStartRecovery)
    public void openRecoveryFragment(){
        mainActivity.addNewFragment(new FragmentRecoveryUser(),
                FRAGMENT_CREATE_NEW_USER, FRAGMENT_RECOVERY);
    }

    private void makeNewUserRequest() {
        if(checkCorrectValues())
            requestManager.addNewUser(getRequestParams())
                    .subscribe(getUserConsumer(), getDefaultErrorConsumer(mainView));
    }

    private boolean checkCorrectValues(){
        if(checkCorrectEditTextValues(editTextName, editTextPassword, editTextPasswordConfirm)){
            if(passwordAreEquals())
                return true;
            else
                makeSnackbarMessage(mainView, R.string.passwords_not_equals);
        }else
            makeSnackbarMessage(mainView, R.string.all_fields_must_be_filled );

        return false;
    }

    private boolean checkCorrectEditTextValues(EditText ... editTextValues){
        return !Stream.of(editTextValues).anyMatch(et -> et.getText().toString().isEmpty());
    }

    private boolean passwordAreEquals(){
        return editTextPassword.getText().toString().equals(
                editTextPasswordConfirm.getText().toString());
    }

    private Map<String, Object> getRequestParams(){
        Map<String, Object> params = new HashMap<>();
        String password = getEncryptedPassword(editTextPassword.getText().toString());
        params.put("name", editTextName.getText().toString().trim());
        params.put("password", password);
        params.put("sex", String.valueOf(getSexFromRadioGroup()));
        params.put("token", accessToken.getToken());
        return params;
    }

    private String getEncryptedPassword(String password){
        try {
            return Crypto.encrypt(password);
        } catch (Exception e) {
            e.printStackTrace();
            return password;
        }
    }

    private int getSexFromRadioGroup() {
        if(radioGroup.getCheckedRadioButtonId() == R.id.rbFragmentStartFemale)
            return 0;
        return 1;
    }

    private Consumer<User> getUserConsumer() {
        return u -> {
            user.copyValues(u);
            experience.updateTotalExp(user.getExp());
            mainActivity.updateOwnerNavigationView();
            openMainFragment();
        };
    }

    private void openMainFragment() {
        mainActivity.openNavigationItem(0);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.fragment_create_new_user);
    }
}
