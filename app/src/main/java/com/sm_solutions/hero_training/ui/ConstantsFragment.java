package com.sm_solutions.hero_training.ui;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sgsr38213 on 03.01.2018.
 */

public class ConstantsFragment {

    public static final String FRAGMENT_RECOVERY = "fragment_recovery";
    public static final String FRAGMENT_LOADING = "fragment_loading";
    public static final String FRAGMENT_CREATE_NEW_USER = "fragment_create_new_user";
    public static final String FRAGMENT_MAIN = "fragment_main";
    public static final String FRAGMENT_QUESTS = "fragment_quests";
    public static final String FRAGMENT_TRAINING = "fragment_training";

    private static final List<String> NAVIGATION_FRAGMENTS_TO_REPLACE = Arrays.asList(
            FRAGMENT_QUESTS,
            FRAGMENT_TRAINING);

    private static final List<String> NAVIGATION_FRAGMENTS_WITH_TITLE = Arrays.asList(
            FRAGMENT_MAIN,
            FRAGMENT_LOADING,
            FRAGMENT_CREATE_NEW_USER,
            FRAGMENT_QUESTS,
            FRAGMENT_TRAINING,
            FRAGMENT_RECOVERY);

    public static boolean isFragmentToReplace(String tag){
        return NAVIGATION_FRAGMENTS_TO_REPLACE.contains(tag);
    }

    public static boolean isFragmentWithTitle(String tag){
        return NAVIGATION_FRAGMENTS_WITH_TITLE.contains(tag);
    }
}
