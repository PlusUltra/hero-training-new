package com.sm_solutions.hero_training.ui.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.view.Window;
import android.view.WindowManager;

import java.util.HashMap;
import java.util.Map;

import static com.sm_solutions.hero_training.ui.fragment.FragmentModel.BUNDLE_BONUS_EXP;
import static com.sm_solutions.hero_training.ui.fragment.FragmentModel.BUNDLE_IS_LEVEL_UP;

/**
 * Created by ultra on 05.03.18.
 */

public class TransparentDialogConfig {

    static final String PARAM_EXP= "exp";
    static final String PARAM_IS_TRAINING_CASE = "is_training_case";
    static final String PARAM_IS_LEVEL_UP = "is_level_up";
    static final String PARAM_NEW_LEVEL= "new_level";
    public static final String PARAM_CALLBACK= "callback";

    protected static <D extends Dialog> D configureDialog(D dialog){
        setWindowParams(dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private static <D extends Dialog> void setWindowParams(D dialog) {
        final Window window = dialog.getWindow();
        try {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }catch (NullPointerException e){e.printStackTrace();}
    }

    public static Map<String, Object> getParamsForDialog(Intent intent, int level,
                                                         boolean isTrainingCase){
        int exp = intent.getIntExtra(BUNDLE_BONUS_EXP, 0);
        boolean isLevelUp = intent.getBooleanExtra(BUNDLE_IS_LEVEL_UP, false);
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_EXP, exp);
        params.put(PARAM_IS_LEVEL_UP, isLevelUp);
        params.put(PARAM_NEW_LEVEL, level);
        params.put(PARAM_IS_TRAINING_CASE, isTrainingCase);
        return params;
    }
}
