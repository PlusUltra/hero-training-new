package com.sm_solutions.hero_training.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_MAIN;
import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_QUESTS;
import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_TRAINING;

/**
 * Created by sgsr38213 on 19.10.2017.
 */

public class FragmentMain extends FragmentModel {

    @BindView(R.id.btnFragmentMainTraining)
    Button btnTraining;
    @BindView(R.id.btnFragmentMainQuests)
    Button btnQuests;
    @BindView(R.id.btnFragmentMainMenu)
    Button btnMenu;
    @BindView(R.id.tvFragmentMainName)
    TextView tvName;
    @BindView(R.id.tvFragmentMainLevel)
    TextView tvLevel;
    @BindView(R.id.tvFragmentMainExp)
    TextView tvExp;
    @BindView(R.id.ivFragmentMainHeroImage)
    ImageView ivHeroMain;
    @BindView(R.id.pbFragmentMainExp)
    ProgressBar pbExp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, v);
        log(user.getName() + " | "
                + experience.getLevel() + " | " + experience.getNextLevelExp());
        setUpExpInfo();
        return v;
    }


    @OnClick(R.id.btnFragmentMainTraining)
    void openTrainingFragment() {
        mainActivity.addNewFragment(new FragmentTraining(), FRAGMENT_MAIN, FRAGMENT_TRAINING);
    }

    @OnClick(R.id.btnFragmentMainQuests)
    void openQuestsFragment() {
        mainActivity.addNewFragment(new FragmentQuests(), FRAGMENT_MAIN, FRAGMENT_QUESTS);
    }

    @OnClick(R.id.btnFragmentMainMenu)
    void openMenuFragment() {
    }

    @Override
    protected void onReceiveBonusExpIntent(Intent broadcastIntent) {
        setUpExpInfo();
    }

    private void setUpExpInfo() {
        tvName.setText(user.getName());
        tvLevel.setText(getString(R.string.level) + " " + experience.getLevel());
        tvExp.setText(experience.getCurrentExp() + " / " + experience.getNextLevelExp());
        pbExp.setMax(experience.getNextLevelExp());
        pbExp.setProgress(experience.getCurrentExp());
    }

    @Override
    protected String getTitle() {
        return getString(R.string.fragment_main);
    }
}
