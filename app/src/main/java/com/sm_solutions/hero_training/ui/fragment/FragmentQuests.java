package com.sm_solutions.hero_training.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.Exercise;
import com.sm_solutions.hero_training.ui.adapters.QuestsAdapter;
import com.sm_solutions.hero_training.ui.custom_views.RecycleDecorations;
import com.sm_solutions.hero_training.ui.dialog.DialogCallback;
import com.sm_solutions.hero_training.ui.dialog.DialogExpGained;
import com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_QUESTS;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_CALLBACK;

public class FragmentQuests extends FragmentModel{

    public static final String BUNDLE_QUEST_CASE = "bundle_quest_case";

    @BindView(R.id.rvFragmentQuestsMain) RecyclerView recyclerView;
    @BindView(R.id.tvFragmentQuestsLabel) TextView tvLabel;
    @BindView(R.id.tvFragmentQuestsExp) TextView tvExp;
    private View mainView;
    private List<Exercise> exerciseList;
    private QuestsAdapter questsAdapter;
    private Exercise intentExercise;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_quests, container, false);
        ButterKnife.bind(this, mainView);
        getQuestsList();
        return mainView;
    }

    private void getQuestsList(){
        new Handler().postDelayed(() -> requestManager
                        .getQuestsList(accessToken.getToken())
                .subscribe(getQuestsConsumer(), getDefaultErrorConsumer(mainView))
                , 200);
    }

    public Consumer<List<Exercise>> getQuestsConsumer() {
        return list -> {
            exerciseList = list;
            Stream.of(exerciseList).forEach(e -> {
                e.setCountDependOnLevel(experience.getLevel());
                e.setExpAndCountDependOnRarity();
            });
            setNewTrainingAdapter();
        };
    }

    private void setNewTrainingAdapter() {
        questsAdapter = new QuestsAdapter(exerciseList);
        questsAdapter.setOpenExerciseListener(getOpenExerciseListener());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new RecycleDecorations.DividerItemDecoration(getContext()));
        recyclerView.setAdapter(questsAdapter);
    }

    private QuestsAdapter.OpenExerciseListener getOpenExerciseListener(){
        return e -> {
            FragmentExercises fragment = new FragmentExercises();
            Bundle bundle = new Bundle();
            bundle.putBoolean(BUNDLE_QUEST_CASE, true);
            bundle.putParcelableArrayList(BUNDLE_EXERCISES,
                    new ArrayList<>(Collections.singletonList(e)));
            fragment.setArguments(bundle);
            mainActivity.addNewFragment(fragment, FRAGMENT_QUESTS, "");
        };
    }

    @Override
    protected void onReceiveBonusExpIntent(Intent broadcastIntent) {
        log("received broadcast");
        rejectIntentExercise(broadcastIntent);
        showDialogs(broadcastIntent);
    }

    private void rejectIntentExercise(Intent broadcastIntent) {
        intentExercise = broadcastIntent.getParcelableExtra(BUNDLE_EXERCISES);
    }

    @Override
    public void updateExerciseAdapter() {
        if(intentExercise.getExp() > 0)
            removeExerciseAfterDelay(intentExercise);
    }

    private void removeExerciseAfterDelay(Exercise exercise) {
        new Handler().postDelayed(() -> {
            removeExerciseById(exercise.getId());
            questsAdapter.notifyDataSetChanged();
            getQuestsList();
        }, 3000);
    }

    private void removeExerciseById(int id) {
        exerciseList.remove(Stream.of(exerciseList).filter(e -> e.getId() == id).findFirst().get());
    }

    private void showDialogs(Intent intent){
        int exp = intent.getIntExtra(BUNDLE_BONUS_EXP, 0);
        if(exp != 0) {
            Map<String, Object> params = TransparentDialogConfig
                    .getParamsForDialog(intent, experience.getLevel(), false);
            params.put(PARAM_CALLBACK, (DialogCallback)(this::updateExerciseAdapter));
            DialogExpGained.showDialog(mainActivity, params);
        }
    }

    @Override
    protected String getTitle() {
        return getString(R.string.fragment_quests);
    }
}
