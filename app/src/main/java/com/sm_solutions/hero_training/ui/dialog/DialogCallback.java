package com.sm_solutions.hero_training.ui.dialog;

/**
 * Created by ultra on 11.03.18.
 */

public interface DialogCallback {

    void call();
}
