package com.sm_solutions.hero_training.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.sm_solutions.hero_training.HeroTrainingApp;
import com.sm_solutions.hero_training.MainActivity;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.new_client.client_impl.RequestManager;
import com.sm_solutions.hero_training.structures.AccessToken;
import com.sm_solutions.hero_training.structures.Experience;
import com.sm_solutions.hero_training.structures.User;
import com.sm_solutions.hero_training.ui.glide.GlideUtil;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.isFragmentWithTitle;


/**
 * Created by sgsr38213 on 19.10.2017.
 */

public abstract class FragmentModel extends Fragment {

    public static final String BROADCAST_BONUS_EXP = "broadcast_bonus_exp";
    public static final String BUNDLE_BONUS_EXP = "bundle_bonus_exp";
    public static final String BUNDLE_IS_LEVEL_UP = "bundle_is_level_up";
    public static final String BUNDLE_EXERCISES = "bundle_exercises";

    @Inject protected User user;
    @Inject protected Experience experience;
    @Inject protected AccessToken accessToken;
    @Inject protected RequestManager requestManager;
    @Inject protected MainActivity mainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDi();
        LocalBroadcastManager.getInstance(mainActivity).registerReceiver(mMessageReceiver,
                new IntentFilter(BROADCAST_BONUS_EXP));
    }

    private void initDi() {
        HeroTrainingApp.get(getContext()).injectFragmentModelComponent(this);
    }

    protected void log(String message){
        Log.d(getClass().getSimpleName(), message);
    }


    @Override
    public void onStart() {
        super.onStart();
        setTitleForMainFragments();
    }

    private void setTitleForMainFragments() {
        if(isFragmentWithTitle(getTag()))
            mainActivity.setTitle(getTitle());
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(mainActivity).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onReceiveBonusExpIntent(intent);
        }
    };

    protected void onReceiveBonusExpIntent(Intent broadcastIntent){}

    protected Consumer<? super Throwable> getDefaultErrorConsumer(View mainView) {
        return t -> {
            t.printStackTrace();
            if(t instanceof HttpException){
                HttpException exception = (HttpException) t;
                if(exception.code() == 500)
                    Snackbar.make(mainView, R.string.server_500_error, Snackbar.LENGTH_SHORT).show();
            }
            if(t instanceof java.net.SocketTimeoutException){
                Snackbar.make(mainView, R.string.internet_error, Snackbar.LENGTH_SHORT).show();
            }
        };
    }

    protected void closeFragment() {
        mainActivity.getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    protected void popUpBackStack(String tag){
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        fm.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected void loadImage(ImageView imageView, String url){
        GlideUtil.loadWithFragment(this, url, imageView);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)
            setTitleForMainFragments();
    }

    protected  String getTitle(){
        return getTag();
    }

    protected void makeSnackbarMessage(View view, int stringId){
        Snackbar.make(view, stringId, Snackbar.LENGTH_SHORT).show();
    }

    public void updateExerciseAdapter(){}
}
