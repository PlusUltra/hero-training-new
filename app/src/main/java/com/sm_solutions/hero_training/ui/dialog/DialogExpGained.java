package com.sm_solutions.hero_training.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_CALLBACK;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_EXP;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_IS_LEVEL_UP;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_IS_TRAINING_CASE;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_NEW_LEVEL;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.configureDialog;

public class DialogExpGained extends Dialog {

    private Map<String, Object> params;

    @BindView(R.id.tvDialogExpCount)
    TextView tvCount;

    @BindView(R.id.tvDialogExpCongrats)
    TextView tvCongrats;

    private boolean isTrainingCase;

    public DialogExpGained(Context context, int myDialog) {
        super(context, myDialog);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        setCongratsText();
        setExpText();
        setOnDismissListener(d -> onDismiss());
    }

    private void bindView() {
        setContentView(R.layout.dialog_finish_exercise);
        ButterKnife.bind(this);
    }

    private void setCongratsText() {
        isTrainingCase = (boolean) params.get(PARAM_IS_TRAINING_CASE);
        String message = getStringById(R.string.congrats) + "\n"
                + getStringById(isTrainingCase
                ? R.string.congrats_training_finish
                : R.string.congrats_quest_finish);
        tvCongrats.setText(message);

    }

    private String getStringById(int id){
        return getContext().getString(id);
    }

    private void setExpText() {
        int expCount = (int) params.get(PARAM_EXP);
        String message = getContext().getString(R.string.exp_gained) + ": " + expCount;
        tvCount.setText(message);
    }

    public static void showDialog(Context context, Map<String, Object> params){
        DialogExpGained dialog = configureDialog(getNewDialog(context));
        dialog.setParams(params);
        dialog.show();
    }

    private static DialogExpGained getNewDialog(Context context){
        return new DialogExpGained(context, R.style.DialogExerciseInfoStyle);
    }

    private void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public void onDismiss() {
        if(isLevelUp())
            showLevelUpDialog();
        else
            ((DialogCallback)params.get(PARAM_CALLBACK)).call();
    }

    private boolean isLevelUp(){
        return (boolean) params.get(PARAM_IS_LEVEL_UP);
    }

    private void showLevelUpDialog() {
        int level = (int) params.get(PARAM_NEW_LEVEL);
        DialogLevelUp.showDialog(getContext(), level, ((DialogCallback)params.get(PARAM_CALLBACK)));
    }
}
