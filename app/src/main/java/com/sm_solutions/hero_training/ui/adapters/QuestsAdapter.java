package com.sm_solutions.hero_training.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.Exercise;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ultra on 15.12.17.
 */

public class QuestsAdapter extends RecyclerView.Adapter<QuestsAdapter.QuestViewHolder>{

    private List<Exercise> exerciseList;
    private OpenExerciseListener exerciseListener;

    public QuestsAdapter(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
        setHasStableIds(true);
    }

    @Override
    public QuestViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_quest , viewGroup, false);
        return new QuestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuestViewHolder viewHolder, int i) {
        Exercise exercise = exerciseList.get(i);

        viewHolder.tvRarity.setText(exercise.getRarity());
        viewHolder.tvType.setText(exercise.getType());
        viewHolder.tvCount.setText(String.valueOf(exercise.getCount()));
        viewHolder.tvExp.setText(String.valueOf(exercise.getExp()));

        viewHolder.itemView.setOnClickListener(v ->
                exerciseListener.openExerciseFragment(exercise));
    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    class QuestViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvItemQuestRarity) TextView tvRarity;
        @BindView(R.id.tvItemQuestType) TextView tvType;
        @BindView(R.id.tvItemQuestCount) TextView tvCount;
        @BindView(R.id.tvItemQuestExp) TextView tvExp;

        public QuestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOpenExerciseListener(OpenExerciseListener listener){
        exerciseListener = listener;
    }


    public interface OpenExerciseListener {
        void openExerciseFragment(Exercise exercise);
    }
}
