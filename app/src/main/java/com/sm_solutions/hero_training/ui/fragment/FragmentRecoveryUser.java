package com.sm_solutions.hero_training.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.crypto.Crypto;
import com.sm_solutions.hero_training.structures.User;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * Created by sgsr38213 on 19.10.2017.
 */

public class FragmentRecoveryUser extends FragmentModel{

    @BindView(R.id.etFragmentRecoveryName) EditText editTextName;
    @BindView(R.id.etFragmentRecoveryPassword) EditText editTextPassword;
    View mainView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mainView = inflater.inflate(R.layout.fragment_recovery, container, false);
        ButterKnife.bind(this, mainView);
        return mainView;
    }

    @OnClick(R.id.btnFragmentRecoveryConfirm)
    void makeRecoveryRequest() {
        if(checkCorrectValues())
            requestManager.recoveryUser(getRequestParams())
                    .subscribe(getUserConsumer(), getDefaultErrorConsumer(mainView));
    }

    private boolean checkCorrectValues(){
        if(checkCorrectEditTextValues(editTextName, editTextPassword)){
            return true;
        }else
            Toast.makeText(mainActivity, "All fields must be filled", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean checkCorrectEditTextValues(EditText ... editTextValues){
        return !Stream.of(editTextValues).anyMatch(et -> et.getText().toString().isEmpty());
    }

    private Map<String, Object> getRequestParams(){
        Map<String, Object> params = new HashMap<>();
        String password = getEncryptedPassword(editTextPassword.getText().toString());
        params.put("name", editTextName.getText().toString().trim());
        params.put("password", password);
        params.put("token", accessToken.getToken());
        return params;
    }

    private String getEncryptedPassword(String password){
        try {
            return Crypto.encrypt(password);
        } catch (Exception e) {
            e.printStackTrace();
            return password;
        }
    }

    private Consumer<User> getUserConsumer() {
        return u -> {
            user.copyValues(u);
            experience.updateTotalExp(user.getExp());
            mainActivity.updateOwnerNavigationView();
            openMainFragment();
        };
    }

    private void openMainFragment() {
        mainActivity.openNavigationItem(0);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.recovery);
    }
}
