package com.sm_solutions.hero_training.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.dto.StatisticDetailsDto;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

//https://github.com/PhilJay/MPAndroidChart/wiki/Getting-Started
public class FragmentStatistic extends FragmentModel {

    @BindView(R.id.chartFragmentStatistic) LineChart chart;
    private View mainView;

    private StatisticDetailsDto statisticDetailsDto;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_statistic, container, false);
        ButterKnife.bind(this, mainView);
        makeChartsDataRequest();
        return mainView;
    }

    private void makeChartsDataRequest() {
        requestManager.getExpStatistic(accessToken.getToken())
                .subscribe(getExpDtoConsumer(), getDefaultErrorConsumer(mainView));
    }

    private void setStatisticDetailsDto(StatisticDetailsDto dto){
        statisticDetailsDto = dto;
    }

    public Consumer<StatisticDetailsDto> getExpDtoConsumer() {
        return dto -> {
            setStatisticDetailsDto(dto);
            fillCharts();
        };
    }

    private void fillCharts() {
        LineDataSet trainingData = new LineDataSet(statisticDetailsDto.getTrainingEntry(), "Training");
        LineDataSet questsData = new LineDataSet(statisticDetailsDto.getQuestsEntry(), "Quests");

        chart.setData(new LineData(trainingData));
        chart.setData(new LineData(questsData));
        chart.invalidate();
    }
}
