package com.sm_solutions.hero_training.ui.glide;

import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.sm_solutions.hero_training.GlideApp;

//https://github.com/bumptech/glide

public class GlideUtil {

    public static void loadWithFragment(Fragment fragment, String url, ImageView imageView){
        GlideApp
                .with(fragment)
                .load(url)
                .centerInside()
                .into(imageView);
    }
}
