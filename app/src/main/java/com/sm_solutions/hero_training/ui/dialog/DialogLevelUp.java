package com.sm_solutions.hero_training.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.configureDialog;

public class DialogLevelUp extends Dialog {

    private int level = 0;

    @BindView(R.id.tvDialogLevelUpNewLevel)
    TextView tvNewLevelText;

    private DialogCallback callback;

    public DialogLevelUp(Context context, int myDialog) {
        super(context, myDialog);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        setLevelText();
        setOnDismissListener(d -> onDismiss());
    }

    private void bindView() {
        setContentView(R.layout.dialog_level_up);
        ButterKnife.bind(this);
    }

    private void setLevelText(){
        String message =  getContext().getString(R.string.congrats_level_is)+ " " + level
                + "\n" + getContext().getString(R.string.congrats_keep_it_up);
        tvNewLevelText.setText(message);
    }

    private void setCallback(DialogCallback callback) {
        this.callback = callback;
    }

    public static void showDialog(Context context, int level, DialogCallback call){
        DialogLevelUp dialog = configureDialog(getNewDialog(context));
        dialog.setLevel(level);
        dialog.setCallback(call);
        dialog.show();
    }

    private static DialogLevelUp getNewDialog(Context context){
        return new DialogLevelUp(context, R.style.DialogExerciseInfoStyle);
    }

    private void setLevel(int level) {
        this.level = level;
    }

    private void onDismiss(){
        callback.call();
    }
}
