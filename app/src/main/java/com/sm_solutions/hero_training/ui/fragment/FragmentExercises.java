package com.sm_solutions.hero_training.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.Exercise;
import com.sm_solutions.hero_training.structures.dto.DtoUtils;
import com.sm_solutions.hero_training.structures.dto.QuestInfoDto;
import com.sm_solutions.hero_training.structures.dto.TrainingInfoDto;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_QUESTS;
import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_TRAINING;
import static com.sm_solutions.hero_training.ui.dialog.DialogExerciseInfo.showDialog;

/**
 * Created by ultra on 16.12.17.
 */

public class FragmentExercises extends FragmentModel {


    private View mainView;
    private ArrayList<Exercise> exercises;
    private int count = 0;
    private int lastExerciseIndex;
    private boolean isQuestCase;

    @BindView(R.id.tvFragmentExercisesType) TextView tvType;
    @BindView(R.id.tvFragmentExercisesCount) TextView tvCount;
    @BindView(R.id.cbFragmentExercisesDone) CheckBox cbDone;
    @BindView(R.id.btnFragmentExercisesNext) Button btnNext;
    @BindView(R.id.ivFragmentExercises) ImageView ivMain;
    @BindView(R.id.btnFragmentExercisesInfo) Button btnInfo;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_exercises, container, false);
        ButterKnife.bind(this, mainView);
        initValues();
        showNextExercise();
        return mainView;
    }

    private void initValues() {
        exercises = getArguments().getParcelableArrayList(BUNDLE_EXERCISES);
        isQuestCase = getArguments().getBoolean(FragmentQuests.BUNDLE_QUEST_CASE, false);
        lastExerciseIndex = exercises.size() - 1;
    }

    private void showNextExercise() {
        Exercise exercise = exercises.get(count);
        String repeat = getString(R.string.repeat);
        String times = getString(R.string.times);
        tvType.setText(exercise.getType());
        tvCount.setText(repeat + " " + exercise.getCount() + " " + times);
        loadImage(ivMain, exercise.getLink());
        setInfoButtonOnClick(exercise);
        if(count == lastExerciseIndex)
            btnNext.setText(R.string.finish);
    }

    private void setInfoButtonOnClick(Exercise exercise) {
        btnInfo.setOnClickListener(v -> showDialog(mainActivity, exercise.getType(),
                exercise.getDescription()));
    }

    @OnClick(R.id.btnFragmentExercisesNext)
    void nextExerciseClick(){
        count++;

        if(count > lastExerciseIndex + 1){
            pushExercisesResults();
            return;
        }

        if(count == lastExerciseIndex + 1) {
            removeExpIfNotDone();
            pushExercisesResults();
            return;
        }

        showNextExercise();
        removeExpIfNotDone();
        cbDone.setChecked(false);
    }

    private void removeExpIfNotDone() {
        if(count != 0 && !cbDone.isChecked())
            exercises.get(count-1).removeExp();
    }

    private void pushExercisesResults() {
        Stream.of(exercises).forEach(e -> System.out.println("Exercise exp " + e.getExp()));
        int sumExp = Stream.of(exercises).mapToInt(Exercise::getExp).sum();
        if(sumExp == 0) {
            stopFragment();
            return;
        }

        if(!isQuestCase)
            finishTraining();
        else
            finishQuest(sumExp);
    }

    private void finishTraining() {
        requestManager.finishTraining(getParams())
                .subscribe(getRequestConsumer(), getDefaultErrorConsumer(mainView));
    }

    private TrainingInfoDto getParams() {
        return DtoUtils.getTrainingInfoDto(exercises, accessToken.getToken());
    }

    private void finishQuest(int sumExp) {
        requestManager.finishQuest(getQuestParams(sumExp))
                .subscribe(getRequestConsumer(), getDefaultErrorConsumer(mainView));
    }

    private QuestInfoDto getQuestParams(int sumExp) {

        QuestInfoDto dto = new QuestInfoDto();
        dto.setToken(accessToken.getToken());
        dto.setExp(sumExp);
        dto.setCount(exercises.get(0).getCount());
        dto.setTraining_id(exercises.get(0).getId());
        return dto;
    }

    private Consumer<JSONObject> getRequestConsumer() {
        return jsonObject -> {
            user.addExp(jsonObject.getInt("exp"));
            boolean isLevelUp = experience.updateTotalExp(user.getExp());
            mainActivity.updateOwnerNavigationView();
            sendBroadcast(jsonObject.getInt("exp"), isLevelUp);
            stopFragment();
        };
    }

    private void sendBroadcast(int exp, boolean isLevelUp) {
        log("Broadcasting message");
        Intent intent = new Intent(BROADCAST_BONUS_EXP);
        intent.putExtra(BUNDLE_BONUS_EXP, exp);
        intent.putExtra(BUNDLE_IS_LEVEL_UP, isLevelUp);
        if(isQuestCase)
            intent.putExtra(BUNDLE_EXERCISES, exercises.get(0));
        else
            intent.putParcelableArrayListExtra(BUNDLE_EXERCISES, exercises);

        LocalBroadcastManager.getInstance(mainActivity).sendBroadcast(intent);
    }

    private void stopFragment() {
        closeFragment();
        popUpBackStack(isQuestCase ? FRAGMENT_QUESTS : FRAGMENT_TRAINING);
    }
}
