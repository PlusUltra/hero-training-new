package com.sm_solutions.hero_training.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.sm_solutions.hero_training.R;
import com.sm_solutions.hero_training.structures.Exercise;
import com.sm_solutions.hero_training.ui.adapters.TrainingAdapter;
import com.sm_solutions.hero_training.ui.custom_views.RecycleDecorations;
import com.sm_solutions.hero_training.ui.dialog.DialogCallback;
import com.sm_solutions.hero_training.ui.dialog.DialogExpGained;
import com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_TRAINING;
import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.PARAM_CALLBACK;

/**
 * Created by sgsr38213 on 19.10.2017.
 */

public class FragmentTraining extends FragmentModel{

    @BindView(R.id.rvFragmentTrainingMain) RecyclerView recyclerView;
    @BindView(R.id.btnFragmentTrainingStart) Button btnStartTraining;
    @BindView(R.id.tvFragmentTrainingLabel) TextView tvLabel;

    View mainView;
    List<Exercise> exerciseList;
    TrainingAdapter trainingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_training, container, false);
        ButterKnife.bind(this, mainView);
        getTrainingList();
        return mainView;
    }

    private void getTrainingList(){
        new Handler().postDelayed(() -> requestManager
                        .getTrainingList(accessToken.getToken())
                .subscribe(getTrainingConsumer(), getDefaultErrorConsumer(mainView)),
                200);
    }

    public Consumer<List<Exercise>> getTrainingConsumer() {
        return list -> {
            exerciseList = list;
            Stream.of(exerciseList).forEach(e -> e.setCountDependOnLevel(experience.getLevel()));
            setNewTrainingAdapter();
        };
    }

    private void setNewTrainingAdapter() {
        trainingAdapter = new TrainingAdapter(exerciseList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new RecycleDecorations.DividerItemDecoration(getContext()));
        recyclerView.setAdapter(trainingAdapter);
    }

    @OnClick(R.id.btnFragmentTrainingStart)
    void startTraining(){
        FragmentExercises fragment = new FragmentExercises();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BUNDLE_EXERCISES, new ArrayList<>(exerciseList));
        fragment.setArguments(bundle);
        mainActivity.addNewFragment(fragment, FRAGMENT_TRAINING, "");
    }

    @Override
    protected void onReceiveBonusExpIntent(Intent broadcastIntent) {
        log("received broadcast");
        updateTrainingAdapter(broadcastIntent);
        updateTextViews();
        setNewButtonClick();
        showDialogs(broadcastIntent);
    }

    private void setNewButtonClick() {
        btnStartTraining.setText(getString(R.string.finish));
        btnStartTraining.setOnClickListener(v -> mainActivity.openNavigationItem(0));
    }

    private void updateTrainingAdapter(Intent broadcastIntent) {
        exerciseList = broadcastIntent.getParcelableArrayListExtra(BUNDLE_EXERCISES);
    }

    @Override
    public void updateExerciseAdapter() {
        trainingAdapter.notifyDataSetChanged();
        new Handler().postDelayed(this::removeZeroExpExercises, 3000);
    }

    private void removeZeroExpExercises() {
        exerciseList = Stream.of(exerciseList).filter(e -> e.getExp() != 0).toList();
        trainingAdapter.setExerciseList(exerciseList);
        trainingAdapter.notifyDataSetChanged();
    }

    private void updateTextViews() {
        tvLabel.setText(tvLabel.getText() + " " + getString(R.string.is_done));
    }

    private void showDialogs(Intent intent){
        int exp = intent.getIntExtra(BUNDLE_BONUS_EXP, 0);
        if(exp != 0) {
            Map<String, Object> params = TransparentDialogConfig
                    .getParamsForDialog(intent, experience.getLevel(), true);
            params.put(PARAM_CALLBACK, (DialogCallback)(this::updateExerciseAdapter));
            DialogExpGained.showDialog(mainActivity, params);
        }
    }

    @Override
    protected String getTitle() {
        return getString(R.string.fragment_training);
    }
}
