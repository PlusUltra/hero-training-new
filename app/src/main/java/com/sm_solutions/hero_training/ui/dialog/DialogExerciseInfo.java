package com.sm_solutions.hero_training.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.widget.TextView;

import com.sm_solutions.hero_training.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sm_solutions.hero_training.ui.dialog.TransparentDialogConfig.configureDialog;

public class DialogExerciseInfo extends Dialog {

    private String type;
    private String description;

    @BindView(R.id.tvDialogExerciseInfoType)
    TextView tvType;

    @BindView(R.id.constraintDialogExerciseDescriptionMain)
    ConstraintLayout layoutMain;

    @BindView(R.id.tvDialogExerciseInfoDescription)
    TextView tvDescription;

    private DialogExerciseInfo(@NonNull Context context) {
        super(context);
    }

    public DialogExerciseInfo(Context context, int style) {
        super(context, style);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        setInfoToTextViews();
    }

    private void bindView() {
        setContentView(R.layout.dialog_exercise_description);
        ButterKnife.bind(this);
    }

    private void setType(String type) {
        this.type = type;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setInfoToTextViews(){
        tvType.setText(type);
        tvDescription.setText(description);
        layoutMain.setOnClickListener(v ->
                DialogExerciseInfo.this.dismiss());
    }

    public static void showDialog(Context context, String type, String description){
        DialogExerciseInfo dialog = getNewDialog(context);
        dialog.setType(type);
        dialog.setDescription(description);
        dialog.show();
    }

    private static DialogExerciseInfo getNewDialog(Context context){
        return configureDialog(new DialogExerciseInfo(context, R.style.DialogExerciseInfoStyle));
    }
}
