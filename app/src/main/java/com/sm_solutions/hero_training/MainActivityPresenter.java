package com.sm_solutions.hero_training;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sm_solutions.hero_training.new_client.client_impl.RequestManager;
import com.sm_solutions.hero_training.structures.Experience;
import com.sm_solutions.hero_training.structures.User;

/**
 * Created by sgsr38213 on 05.12.2017.
 */

public class MainActivityPresenter {

    private MainActivity mainActivity;
    private Authorization authorization;
    private RequestManager requestManager;
    private User owner = new User();
    private Experience experience = new Experience();
    private FragmentTransactionUtils fragmentTransactionUtils;


    public MainActivityPresenter(MainActivity mainActivity, RequestManager requestManager,
                                 Authorization authorization) {
        this.mainActivity = mainActivity;
        this.requestManager = requestManager;
        this.authorization = authorization;
    }

    public void initOwnerInfo(){
       authorization.initAuthorization(user -> {
           owner.copyValues(user);
           experience.updateTotalExp(owner.getExp());
           updateOwnerNavigationView();
           mainActivity.openNavigationItem(0);
       });
    }

    public User getOwner() {
        return owner;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setFragmentTransactionUtils(FragmentTransactionUtils fragmentTransactionUtils) {
        this.fragmentTransactionUtils = fragmentTransactionUtils;
    }

    public void addNewFragment(Fragment fragment, String currentFragmentTag, String newTag) {
        fragmentTransactionUtils.addNewFragment(fragment, currentFragmentTag, newTag);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        fragmentTransactionUtils.selectDrawerItem(menuItem);
    }

    public void updateOwnerNavigationView(){
        View header = mainActivity.navigationView.getHeaderView(0);
        TextView tvName = header.findViewById(R.id.tvNavigationName);
        TextView tvLevel = header.findViewById(R.id.tvNavigationLevel);

        tvName.setText(owner.getName());
        tvLevel.setText(mainActivity.getString(R.string.level) + " " + experience.getLevel());
    }

    public boolean shouldReplaceFragment(){
        return fragmentTransactionUtils.shouldReplaceWithMainFragment();
    }

    public void openNavigationItem(NavigationView navigationView, int item){
        fragmentTransactionUtils.openNavigationItem(navigationView, item);
    }
}
