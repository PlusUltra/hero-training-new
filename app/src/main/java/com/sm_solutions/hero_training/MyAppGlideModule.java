package com.sm_solutions.hero_training;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by ultra on 24.02.18.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}