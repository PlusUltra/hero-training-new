package com.sm_solutions.hero_training.crypto;

/**
 * Created by ultra on 06.03.18.
 */

class KeyGenerator {

    public static String getKey(long a, long b){
        long value = (long) (Math.log(a*b) * (a + b));
        String key = String.valueOf(value);
        key = formatStringForKey(key);
        //Log.d("key", key);
        return key;
    }

    public static byte[] getKeyForCustomString(String key){
        int length = key.length();
        int part = length % 2 == 1 ? length/2 + 1 : length/2;
        key = key.substring(part, length) + key.substring(0, part);
        return formatUTF(key);
    }

    private static byte[] formatUTF(String key) {
        byte[] bytes = formatStringForKey(key).getBytes();
        byte[] keyBytes = new byte[16];

        for(int i = 0; i < bytes.length; i++){
            if(i < 16)
                keyBytes[i] = bytes[i];
            else
                break;
        }
        return keyBytes;
    }

    public static byte[] getIvForCustomString(String key){
        return formatUTF(key);
    }

    public static String getIv(long a, long b){
        long value = (long) (Math.log(a + b) * (a * b));
        String iv = String.valueOf(value);
        iv = formatStringForKey(iv);
        //Log.d("iv", iv);
        return iv;
    }

    public static String getKeyChat(long id){
        long value = (long) (Math.log(Math.atan(id)) + id/13)*id;
        String key = String.valueOf(value);
        key = formatStringForKey(key);
        // Log.d("key", key);
        return key;
    }

    public static String getIvChat(long id){
        long value = (long) (Math.log(id*13) * 169 * id);
        String iv = String.valueOf(value);
        iv = formatStringForKey(iv);
        //Log.d("iv", iv);
        return iv;
    }

    private static String formatStringForKey(String str){
        if(str.length() > 16){
            str = str.substring(0, 16);
        }else{
            while (str.length() < 16){
                str += "0";
            }
        }
        return str;
    }
}