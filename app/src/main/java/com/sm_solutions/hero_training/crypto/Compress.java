package com.sm_solutions.hero_training.crypto;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by ultra on 06.03.18.
 */
class Compress {

    public static String compressString(byte[] srcTxt)
            throws IOException {
        ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(rstBao);
        zos.write(srcTxt);
        IOUtils.closeQuietly(zos);

        byte[] bytes = rstBao.toByteArray();
        return new String(Base64.encodeBase64(bytes));
    }

    public static byte[] uncompressString(String zippedBase64Str)
            throws IOException {
        byte[] result = null;
        byte[] bytes = Base64.decodeBase64(zippedBase64Str.getBytes());
        GZIPInputStream zi = null;
        try {
            zi = new GZIPInputStream(new ByteArrayInputStream(bytes));
            result = IOUtils.toByteArray(zi);
        } finally {
            IOUtils.closeQuietly(zi);
        }
        return result;
    }
}