package com.sm_solutions.hero_training.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ultra on 06.03.18.
 */

public class Crypto {
    private static final long ONE = 175369851;
    private static final long TWO = 856932475;
    private static byte[] generatedKey = KeyGenerator.getKey(ONE, TWO).getBytes();
    private static byte[] generatedIv = KeyGenerator.getIv(ONE, TWO).getBytes();

    public static String decrypt(String stringToDecrypt)  {
        byte[] outText = new byte[0];
        try {
            outText = decryption(stringToDecrypt);
        }catch (Exception ignored){}

        return new String(outText).trim();
    }

    private static byte[] decryption (String stringToDecrypt) throws  Exception{
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(generatedKey, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(generatedIv);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        return cipher.doFinal(Compress.uncompressString(stringToDecrypt));
    }

    public static String encrypt(String stringToEncrypt) throws Exception{
        stringToEncrypt = stringToEncrypt.trim();
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(generatedKey, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(generatedIv);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        return Compress.compressString(cipher.doFinal(stringToEncrypt.getBytes("UTF-8")));
    }
}
