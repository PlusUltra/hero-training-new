package com.sm_solutions.hero_training;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sm_solutions.hero_training.new_client.client_impl.RequestManager;
import com.sm_solutions.hero_training.structures.AccessToken;
import com.sm_solutions.hero_training.structures.User;
import com.sm_solutions.hero_training.structures.dto.UserTokenDto;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import io.reactivex.functions.Consumer;
import retrofit2.HttpException;


public class Authorization {

    private static final String TOKEN_PREFERENCES = "token_preferences";
    private static final String TAG = "Authorization";
    private MainActivity activity;
    private RequestManager requestManager;
    private AccessToken token = new AccessToken("");
    private Consumer<User> userConsumer;

    public Authorization(MainActivity activity, RequestManager requestManager) {
        this.requestManager = requestManager;
        this.activity = activity;
    }

    public void initAuthorization(Consumer<User> consumer){
        userConsumer = consumer;
        new Handler().postDelayed(this::start, getTokenFromPreferences() == null ? 2500 : 0);
    }

    private void start() {
        getFirebaseToken();
        if(token.getToken() != null)
            makeUserRequest();
        else
            Toast.makeText(activity, "Tokens are null", Toast.LENGTH_SHORT).show();
    }

    private void subscribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("new_quests");
    }

    private void getFirebaseToken(){
        String newToken = FirebaseInstanceId.getInstance().getToken();
        token.setToken(newToken);
        Log.d(TAG, "Is token null? : " + (newToken == null) + " | " + newToken);
    }

    private void makeUserRequest(){
        String tokenFromPreferences = getTokenFromPreferences();
        if(tokenFromPreferences == null){
            saveNewTokens();
            activity.openStartFragment();
        }else {
            if(!tokenFromPreferences.equals(token.getToken())) {
                saveNewTokens();
                requestManager.updateToken(getUserTokenDto(tokenFromPreferences))
                        .subscribe(userConsumer, getTokensErrorConsumer());
            }else
                requestManager.getUserInstanceRequest(urlEncode(token.getToken()))
                        .subscribe(userConsumer, getUserErrorConsumer());

        }
    }

    private UserTokenDto getUserTokenDto(String tokenFromPreferences) {
        return new UserTokenDto(tokenFromPreferences, token.getToken());
    }

    private Consumer<? super Throwable> getUserErrorConsumer(){
        return throwable -> {
            throwable.printStackTrace();
            if(throwable instanceof HttpException){
                HttpException exception = (HttpException) throwable;
                if(exception.code() == 500) {
                    Toast.makeText(activity, R.string.server_500_error, Toast.LENGTH_SHORT).show();
                    activity.openStartFragment();
                }
            }

            if(throwable instanceof IOException){
                Toast.makeText(activity, R.string.internet_error, Toast.LENGTH_SHORT).show();
                activity.openStartFragment();
            }
        };
    }

    private Consumer<? super Throwable> getTokensErrorConsumer(){
        return throwable -> {
            throwable.printStackTrace();
            activity.openStartFragment();
        };
    }

    public void saveNewTokens() {
        Log.d(TAG, "saving new token " + token.getToken());
        if(areTokensNotEquals())
            getSharedPreferences(activity)
                    .edit().putString(TOKEN_PREFERENCES, token.getToken()).apply();
        else
            Log.d(TAG, "tokens are the same or null: " + token);
    }

    private boolean areTokensNotEquals() {
        String prefToken = getTokenFromPreferences();
        return prefToken == null || !prefToken.equals(token.getToken());
    }

    private String getTokenFromPreferences(){
        String tokenFromPrefs = getSharedPreferences(activity).getString(TOKEN_PREFERENCES, null);
        Log.d(TAG, "get shared token " + tokenFromPrefs);
        return tokenFromPrefs;
    }

    private SharedPreferences getSharedPreferences(Context context){
        return ((MainActivity)context)
                .getPreferences(Context.MODE_PRIVATE);
    }

    public AccessToken getToken() {
        return token;
    }

    private String urlEncode(String userToken){
        try {
            return URLEncoder.encode(userToken, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return userToken;
        }
    }
}
