package com.sm_solutions.hero_training;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.sm_solutions.hero_training.ui.fragment.FragmentCreateNewUser;
import com.sm_solutions.hero_training.ui.fragment.FragmentLoading;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_CREATE_NEW_USER;
import static com.sm_solutions.hero_training.ui.ConstantsFragment.FRAGMENT_LOADING;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.frameContainer) FrameLayout frameLayout;

    @Inject
    public MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        diInit();
        presenter.initOwnerInfo();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setupDrawerContent();
    }

    private void diInit() {
        HeroTrainingApp.get(this).injectMainActivityComponent(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            checkToReplaceMainFragment();
        }
    }

    private void checkToReplaceMainFragment() {
        if(presenter.shouldReplaceFragment())
            openNavigationItem(0);
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public void openStartFragment() {
        replaceFragment(new FragmentCreateNewUser(), FRAGMENT_CREATE_NEW_USER);
    }

    private void replaceFragment(Fragment fragment, String tag){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).commit();
    }

    public void addNewFragment(Fragment fragment, String currentFragmentTag, String newTag){
        presenter.addNewFragment(fragment, currentFragmentTag, newTag);
    }

    private void setupDrawerContent() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
                });
        setFirstItemNavigationView();
    }

    public void selectDrawerItem(MenuItem menuItem) {
        presenter.selectDrawerItem(menuItem);
        drawer.closeDrawers();
    }

    private void setFirstItemNavigationView() {
        replaceFragment(new FragmentLoading(), FRAGMENT_LOADING);
    }

    public void updateOwnerNavigationView(){
        presenter.updateOwnerNavigationView();
    }

    public void openNavigationItem(int item){
        presenter.openNavigationItem(navigationView, item);
    }
}
