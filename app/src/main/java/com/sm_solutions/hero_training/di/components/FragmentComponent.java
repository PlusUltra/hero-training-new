package com.sm_solutions.hero_training.di.components;

import com.sm_solutions.hero_training.di.modules.FragmentModule;
import com.sm_solutions.hero_training.di.scope.FragmentModelScope;
import com.sm_solutions.hero_training.ui.fragment.FragmentModel;

import dagger.Subcomponent;

@FragmentModelScope
@Subcomponent(modules = FragmentModule.class)

public interface FragmentComponent {
    void inject(FragmentModel fragmentModel);
}