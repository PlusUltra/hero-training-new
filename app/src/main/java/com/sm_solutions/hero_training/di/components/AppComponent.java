package com.sm_solutions.hero_training.di.components;

import com.sm_solutions.hero_training.HeroTrainingApp;
import com.sm_solutions.hero_training.di.modules.AppModule;
import com.sm_solutions.hero_training.di.modules.MainActivityModule;
import com.sm_solutions.hero_training.di.modules.NetModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, NetModule.class})
@Singleton
public interface AppComponent {

    MainActivityComponent plus(MainActivityModule mainActivityModule);
    void inject(HeroTrainingApp application);
}
