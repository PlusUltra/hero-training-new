package com.sm_solutions.hero_training.di.modules;

import com.sm_solutions.hero_training.ui.fragment.FragmentModel;

import dagger.Module;

/**
 * Created by Arthas on 06.09.2017.
 */

@Module
public class FragmentModule {

    private FragmentModel fragmentModel;

    public FragmentModule(FragmentModel fragmentModel) {
        this.fragmentModel = fragmentModel;
    }

}
