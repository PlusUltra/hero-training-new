package com.sm_solutions.hero_training.di.modules;

import com.sm_solutions.hero_training.Authorization;
import com.sm_solutions.hero_training.FragmentTransactionUtils;
import com.sm_solutions.hero_training.MainActivity;
import com.sm_solutions.hero_training.MainActivityPresenter;
import com.sm_solutions.hero_training.di.scope.MainActivityScope;
import com.sm_solutions.hero_training.new_client.client_impl.RequestManager;
import com.sm_solutions.hero_training.new_client.client_impl.RequestsService;
import com.sm_solutions.hero_training.structures.AccessToken;
import com.sm_solutions.hero_training.structures.Experience;
import com.sm_solutions.hero_training.structures.User;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Arthas on 06.09.2017.
 */

@Module
public class MainActivityModule {

    private MainActivity mActivity;

    public MainActivityModule(MainActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @MainActivityScope
    public MainActivity getActivity(){
        return mActivity;
    }


    @Provides
    @MainActivityScope
    public RequestManager getRequestManager(Retrofit retrofit){
        RequestsService service = retrofit.create(RequestsService.class);
        return new RequestManager(service);
    }

    @Provides
    @MainActivityScope
    public Authorization getAuthorization(MainActivity activity, RequestManager requestManager){
        return new Authorization(activity, requestManager);
    }

    @Provides
    @MainActivityScope
    public MainActivityPresenter getPresenter(MainActivity activity, RequestManager requestManager,
                                              Authorization authorization){
        MainActivityPresenter presenter
                = new MainActivityPresenter(activity, requestManager, authorization);
        presenter.setFragmentTransactionUtils(getFragmentTransactionUtils(activity));
        return presenter;
    }

    @Provides
    @MainActivityScope
    public FragmentTransactionUtils getFragmentTransactionUtils(MainActivity activity){
        return new FragmentTransactionUtils(activity);
    }

    @Provides
    @MainActivityScope
    public User getOwner(MainActivityPresenter presenter){
        return presenter.getOwner();
    }

    @Provides
    @MainActivityScope
    public AccessToken getToken(Authorization authorization){
        return authorization.getToken();
    }

    @Provides
    @MainActivityScope
    public Experience getExperience(MainActivityPresenter presenter){
        return presenter.getExperience();
    }


}
