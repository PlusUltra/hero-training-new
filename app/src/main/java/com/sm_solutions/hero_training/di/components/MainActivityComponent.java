package com.sm_solutions.hero_training.di.components;

import com.sm_solutions.hero_training.MainActivity;
import com.sm_solutions.hero_training.di.modules.FragmentModule;
import com.sm_solutions.hero_training.di.modules.MainActivityModule;
import com.sm_solutions.hero_training.di.scope.MainActivityScope;

import dagger.Subcomponent;

@MainActivityScope
@Subcomponent(modules = MainActivityModule.class)

public interface MainActivityComponent {

    void inject(MainActivity mainActivity);
    FragmentComponent plusFragmentComponent(FragmentModule fragmentModule);
}