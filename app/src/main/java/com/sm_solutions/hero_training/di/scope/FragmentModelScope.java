package com.sm_solutions.hero_training.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Arthas on 06.09.2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentModelScope {
}
