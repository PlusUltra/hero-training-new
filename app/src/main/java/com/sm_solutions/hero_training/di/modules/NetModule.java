package com.sm_solutions.hero_training.di.modules;

import com.sm_solutions.hero_training.new_client.NetClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;


/**
 * Created by Arthas on 06.09.2017.
 */

@Module
public class NetModule {
// http://node46363-vertx.mircloud.host

    private static final String BASE_URL = "http://192.168.0.102:8080";

    @Provides
    @Singleton
    public Retrofit getRetrofit(){
        return NetClient.getRetrofit(BASE_URL);
    }
}
